# README #

Framework for a Slick2DGame

Version 1.0:
 - Main menu
 - Options menu
 - Functionality for mouse and keyboard
 - Game logo
 - Bitmap font
 - Play screen
 - Home button
 - Score/Timer
 - Highscore (file)
 - Game over screen
 - Localisation
 - Language menu