package core;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Localisation {
	private static final String BUNDLE_NAME = "locale.texts";

	private static ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_NAME);

	private Localisation() {
	}

	/**
	 * Returns a translated string using the locale.
	 *
	 * @param key
	 *            the resource key of which to return the translated string
	 * @return a translated string
	 */
	public static String getString(final String key) {
		try {
			return bundle.getString(key);
		} catch (MissingResourceException e) {
			return null;
		}
	}

	public static void setLocale(Locale locale) {
		bundle = ResourceBundle.getBundle(BUNDLE_NAME, locale);
	}
	
	public static Locale getLocale() {
		return bundle.getLocale();
	}
}
