package core;

public class Timer {
	private static final int NANO = 1000000000;
	private long startTime;
	private long stopTime;
	private int seconds;
	private boolean isCountDown;
	private boolean isRunning;
	
	public Timer(int seconds) {
		reset(seconds);
		isRunning = false;
	}

	public void start() {
		long difTime = startTime - stopTime;
		stopTime = 0;
		isRunning = true;
		startTime = System.nanoTime() + difTime;
	}
	
	public void pause() {
		isRunning = false;
		stopTime = System.nanoTime();
	}
	
	public void reset(int seconds) {
		isRunning = true;
		this.seconds = seconds;
		if (seconds == 0) isCountDown = false;
		else isCountDown = true;
		startTime = System.nanoTime();
	}
	
	public int getSeconds() {
		return seconds;
	}
	
	public void update() {
		if (isRunning) {
			long actTime = System.nanoTime();
			if (actTime - NANO >= startTime) {
				startTime += NANO;
				if (isCountDown && seconds > 0) seconds--;
				else if (isCountDown) {
					seconds = 0;
					isRunning = false;
				}
				else seconds++;
			}
		}
	}
}
