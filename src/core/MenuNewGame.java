package core;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.Log;

import ui.UiText;
import ui.UiTextButton;
import ui.UiTextLabel;
import ui.UiTextField;

public class MenuNewGame extends Menu {
	private UiTextField nameField;

	public MenuNewGame(final int state) {
		super(state);
	}

	@Override
	public void init(final GameContainer gc, final StateBasedGame sbg) throws SlickException {
		menu.add(new UiTextLabel(Localisation.getString("MenuNewGame.3"), 0, 0)); //Label Your Name
		nameField = new UiTextField(Localisation.getString("MenuNewGame.4"), gc, 0, 0); //Name
		menu.add(nameField);
		menu.add(new UiTextButton(Localisation.getString("MenuNewGame.5"), 0, 0)); //Start Game
		menu.add(new UiTextButton(Localisation.getString("MenuNewGame.6"), 0, 0)); //Return
		menu.add(new UiText(Localisation.getString("MenuNewGame.7"), 0, 0, 100, 32));
		menu.add(new UiText(Localisation.getString("MenuNewGame.8"), 0, 0, 100, 32));
		menu.add(new UiText(Localisation.getString("MenuNewGame.9"), 0, 0, 100, 32));
		menu.add(new UiText(Localisation.getString("MenuNewGame.10"), 0, 0, 100, 32));
		setPositions();
		selection = page0Size+1; //Name
		Log.debug("MenuNewGame.init()");
	}

	@Override
	public void render(final GameContainer gc, final StateBasedGame sbg, final Graphics g) throws SlickException {
		super.render(gc, sbg, g);
	}

	@Override
	public void update(final GameContainer gc, final StateBasedGame sbg, final int dt) throws SlickException {
		super.update(gc, sbg, dt);
		if (mouseClicked || keyEnterPressed) {
			switch (selection) {
			case 1:
				curPage--;
				if (curPage < 1) curPage = 1;
				break;
			case 2:
				curPage++;
				if (curPage > lastPage) curPage = lastPage;
				break;
			case 4: 
				if(1 < menu.size()) menu.get(1).setFocus(true); break;
			case 5:
				Game.score = new Score();
				Game.score.setName(nameField.getText());
				sbg.enterState(Game.PLAY); break;
			case 6: 
				if (Game.score != null) Game.score.setName(nameField.getText());
				sbg.enterState(Game.MENU_MAIN); break;
			default: break;
			}
		}
		if (keyEscPressed) {
			sbg.enterState(Game.MENU_MAIN);
			keyEscPressed = false;
		}
		keyEnterPressed = false;
	}

}
