package core;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.Log;

import ui.UiButton;
import ui.UiTextButton;
import ui.UiTextLabel;
import ui.UiTextObject;

public class MenuGraphics extends Menu {
	//index of the buttons
	private final int I_FPS = 7;
	private final int I_VSYNC = 8;

	private DisplayMode[] displayModes;
	private String[] displayModesText;
	private UiTextLabel labelMode;
	private UiTextButton btnFPS;
	private UiTextButton btnVSync;
	private int currentMode;
	private int desktopMode;
	private int selectedMode;
	private int lastMode;
	private boolean fpsOn;
	private boolean vSyncOn;

	public MenuGraphics(final int state) {
		super(state);
	}

	@Override
	public void init(final GameContainer gc, final StateBasedGame sbg) throws SlickException {
		setDisplayModesAndTexts();
		fpsOn = Game.showFPS;
		vSyncOn = Game.vSync;
		menu.add(new UiTextLabel(Localisation.getString("MenuGraphics.3"), 0, 0, 
				UiTextObject.H_CENTER, UiTextObject.V_CENTER)); //Label Resolution
		labelMode = new UiTextLabel("", 0, 0, UiTextObject.H_CENTER, UiTextObject.V_CENTER);
		menu.add(labelMode); //4 Resolution
		setLblModeText();
		// set position to -1 for manual settings
		UiButton btnBW = new UiButton("backward",-1,0); //5
		menu.add(btnBW);
		UiButton btnFW = new UiButton("forward",-1,0); //6
		menu.add(btnFW);
		btnFPS = new UiTextButton("", 0, 0);
		setBtnFpsText();
		menu.add(btnFPS); //7 Show FPS
		btnVSync = new UiTextButton("", 0, 0);
		setBtnVSyncText();
		menu.add(btnVSync); //8 VSync
		menu.add(new UiTextButton(Localisation.getString("MenuGraphics.9"), 0, 0)); //Accept
		menu.add(new UiTextButton(Localisation.getString("MenuGraphics.10"), 0, 0)); //Return
		setPositions();
		//manual position left and right from labelMode
		btnBW.setPosition(labelMode.getX()-btnBW.getWidth(), labelMode.getY());
		btnFW.setPosition(labelMode.getX()+labelMode.getWidth(), labelMode.getY());
		selection = 10;
		Log.debug("MenuGraphics.init()");
	}

	private void setDisplayModesAndTexts() {
		try {
			DisplayMode[] modes = Display.getAvailableDisplayModes();
			DisplayMode deskMode = Display.getDesktopDisplayMode();
			int deskFreq = deskMode.getFrequency();
			int deskBPP = deskMode.getBitsPerPixel();
			DisplayMode dm = null;
			//choose only modes with default desktop frequency and bits per pixel
			//and calculate the array size 
			//(its faster to sort an array than an ArrayList)
			int newSize = 0;
			for (int i=0; i<modes.length; i++) {
				dm = modes[i];
				if (dm.getFrequency() == deskFreq && dm.getBitsPerPixel() == deskBPP) {
					newSize++;
				}
			}
			//initialize the arrays with calculated size
			displayModes = new DisplayMode[newSize];
			displayModesText = new String[newSize];
			int index = 0;
			for (int i=0; i<modes.length; i++) {
				dm = modes[i];
				//check default desktop frequency and bits per pixel again
				//and fill the array
				if (dm.getFrequency() == deskFreq && dm.getBitsPerPixel() == deskBPP) {
					displayModes[index] = modes[i];
					index++;
				}
			}
			//sort the array
			sortDisplayModes();
			//fill the array for display mode texts
			//and get current mode
			int w,h;
			for (int i=0; i<displayModes.length; i++) {
				dm = displayModes[i];
				w = dm.getWidth();
				h = dm.getHeight();
				displayModesText[i] = w+"x"+h;
				if (w == Game.width && h == Game.height) {
					currentMode = i;
					selectedMode = i;
				}
				if (w == deskMode.getWidth() && h == deskMode.getHeight()) {
					desktopMode = i;
				}
				Log.debug(dm.toString());
			}
			lastMode = displayModes.length-1;
		} catch (LWJGLException e) {
			Log.error("Error loading display modes!", e);
		}
	}
	
	private void sortDisplayModes() {
		//simple bubble sort
		DisplayMode dm1,dm2,temp;
		boolean notSorted = true;
		int m1,m2;
		while (notSorted) {
			notSorted = false;
			for (int i=0; i<displayModes.length-1; i++) {
				dm1 = displayModes[i];
				dm2 = displayModes[i+1];
				m1 = dm1.getWidth()*10000+dm1.getHeight();
				m2 = dm2.getWidth()*10000+dm2.getHeight();
				if (m1 > m2) {
					temp = dm1;
					displayModes[i] = dm2;
					displayModes[i+1] = temp;
					notSorted = true;
				}
			}
		}
	}
	
	private void setLblModeText() {
		labelMode.setText(displayModesText[selectedMode]);
		if (selectedMode == currentMode) labelMode.setClText(Game.CL_TXT_NORMAL);
		else labelMode.setClText(Game.CL_TXT_CHANGED);
	}

	private void setBtnFpsText() {
		if (fpsOn) {
			btnFPS.setText(Localisation.getString("MenuGraphics."+I_FPS)
					+Localisation.getString("MenuOn"));
		}else{
			btnFPS.setText(Localisation.getString("MenuGraphics."+I_FPS)
					+Localisation.getString("MenuOff"));
		}
		if (selection == I_FPS) btnFPS.setClText(Game.CL_TXT_HOVER);
		else if (fpsOn != Game.showFPS) btnFPS.setClText(Game.CL_TXT_CHANGED);
		else btnFPS.setClText(Game.CL_TXT_NORMAL);
	}
	
	private void setBtnVSyncText() {
		if (vSyncOn) {
			btnVSync.setText(Localisation.getString("MenuGraphics."+I_VSYNC)
					+Localisation.getString("MenuOn"));
		}else{
			btnVSync.setText(Localisation.getString("MenuGraphics."+I_VSYNC)
					+Localisation.getString("MenuOff"));
		}
		if (selection == I_VSYNC) btnVSync.setClText(Game.CL_TXT_HOVER);
		else if (vSyncOn != Game.vSync) btnVSync.setClText(Game.CL_TXT_CHANGED);
		else btnVSync.setClText(Game.CL_TXT_NORMAL);
	}

	private void acceptChanges(final GameContainer gc) throws SlickException {
		if (fpsOn != Game.showFPS) {
			Game.showFPS = fpsOn;
			gc.setShowFPS(fpsOn);
		}
		if (vSyncOn != Game.vSync) {
			Game.vSync = vSyncOn;
			gc.setVSync(vSyncOn);
		}
		if (selectedMode != currentMode) {
			currentMode = selectedMode;
			Game.width = displayModes[selectedMode].getWidth();
			Game.height = displayModes[selectedMode].getHeight();
			boolean fullscreen = false;
			if (selectedMode == desktopMode) fullscreen = true;
			Game.app.setDisplayMode(Game.width, Game.height, fullscreen);
			Game.gameState = Game.NEW_GAME;
			Game.gameIsRunning = false;
			gc.reinit();
		}
	}
	
	@Override
	public void render(final GameContainer gc, final StateBasedGame sbg, final Graphics g) throws SlickException {
		super.render(gc, sbg, g);
	}

	@Override
	public void update(final GameContainer gc, final StateBasedGame sbg, final int dt) throws SlickException {
		super.update(gc, sbg, dt);
		if (mouseClicked || keyEnterPressed) {
			switch (selection) {
			case 1:
				curPage--;
				if (curPage < 1) curPage = 1;
				break;
			case 2:
				curPage++;
				if (curPage > lastPage) curPage = lastPage;
				break;
			case 5: 
				selectedMode--;
				if (selectedMode < 0) selectedMode = 0;
				setLblModeText();
				break;
			case 6: 
				selectedMode++;
				if (selectedMode > lastMode) selectedMode = lastMode;
				setLblModeText();
				break;
			case 7:	fpsOn = !fpsOn;	break;
			case 8: vSyncOn = !vSyncOn;	break;
			case 9: acceptChanges(gc); break;
			case 10: sbg.enterState(Game.MENU_OPTIONS) ;break;
			default: break;
			}
			mouseClicked = false;
			keyEnterPressed = false;
		}
		if (keyEscPressed) {
			sbg.enterState(Game.MENU_MAIN);
			keyEscPressed = false;
		}
		setBtnFpsText();
		setBtnVSyncText();
	}
	
}
