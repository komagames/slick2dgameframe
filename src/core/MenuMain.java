package core;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.Log;

import ui.UiTextButton;

public class MenuMain extends Menu{
	private UiTextButton btnResume;

	public MenuMain(final int state) {
		super(state);
	}

	@Override
	public void init(final GameContainer gc, final StateBasedGame sbg) throws SlickException {
		btnResume = new UiTextButton(Localisation.getString("MenuMain.3"), 0, 0); //Resume
		btnResume.setVisible(false);
		menu.add(btnResume);
		menu.add(new UiTextButton(Localisation.getString("MenuMain.4"), 0, 0)); //New Game
		menu.add(new UiTextButton(Localisation.getString("MenuMain.5"), 0, 0)); //Save
		menu.add(new UiTextButton(Localisation.getString("MenuMain.6"), 0, 0)); //Load
		menu.add(new UiTextButton(Localisation.getString("MenuMain.7"), 0, 0)); //Options
		menu.add(new UiTextButton(Localisation.getString("MenuMain.8"), 0, 0)); //Highscore
		menu.add(new UiTextButton(Localisation.getString("MenuMain.9"), 0, 0)); //Exit
		setPositions();
		selection = 4; //New Game
		Log.debug("MenuMain.init()");
	}
	
	@Override
	public void render(final GameContainer gc, final StateBasedGame sbg, final Graphics g) throws SlickException {
		super.render(gc, sbg, g);
	}

	@Override
	public void update(final GameContainer gc, final StateBasedGame sbg, final int dt) throws SlickException {
		if (Game.gameIsRunning) btnResume.setVisible(true);
		else btnResume.setVisible(false); 
		super.update(gc, sbg, dt);
		if (mouseClicked || keyEnterPressed) {
			switch (selection) {
			case 1:
				curPage--;
				if (curPage < 1) curPage = 1;
				break;
			case 2:
				curPage++;
				if (curPage > lastPage) curPage = lastPage;
				break;
			case 3: Game.gameState = Game.RESUME;
					sbg.enterState(Game.PLAY); break;
			case 4: Game.gameState = Game.NEW_GAME;
					sbg.enterState(Game.NEW_GAME); break;
			case 5: break;
			case 6: sbg.enterState(Game.MENU_LOAD); break;
			case 7: sbg.enterState(Game.MENU_OPTIONS); break;
			case 8: sbg.enterState(Game.HIGHSCORE); break;
			case 9: Game.closeGame(gc); break;
			default: break;
			}
			keyEnterPressed = false;
		}
	}
	
}
