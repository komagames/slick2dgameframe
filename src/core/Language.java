package core;

import java.util.Locale;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.Log;

import ui.UiTextButton;
import ui.UiTextLabel;

public class Language extends Menu{
	private UiTextLabel language;

	public Language(int state) {
		super(state);
	}

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		language = new UiTextLabel(Localisation.getString("Language.3"), 0, 0); //Label
		menu.add(language);
		menu.add(new UiTextButton(Localisation.getString("Language.4"), 0, 0)); //German
		menu.add(new UiTextButton(Localisation.getString("Language.5"), 0, 0)); //English
		menu.add(new UiTextButton(Localisation.getString("Language.6"), 0, 0)); //Return
		setPositions();
		selection = 4; //German
		Log.debug("Language.init()");
	}

	@Override
	public void update(final GameContainer gc, final StateBasedGame sbg, final int dt) throws SlickException {
		super.update(gc, sbg, dt);
		if (mouseClicked || keyEnterPressed) {
			switch (selection) {
			case 1:
				curPage--;
				if (curPage < 1) curPage = 1;
				break;
			case 2:
				curPage++;
				if (curPage > lastPage) curPage = lastPage;
				break;
			case 4: setLocale(Locale.GERMAN); break;
			case 5: setLocale(Locale.ENGLISH); break;
			case 6: sbg.enterState(Game.MENU_OPTIONS); break;
			default: break;
			}
			mouseClicked = false;
			keyEnterPressed = false;
		}
		if (keyEscPressed) {
			sbg.enterState(Game.MENU_OPTIONS);
			keyEscPressed = false;
		}
	}
}
