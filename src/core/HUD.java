package core;

import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import ui.UiButton;
import ui.UiObject;
import ui.UiText;
import ui.UiTextLabel;

public abstract class HUD extends BasicGameState {
	ArrayList<UiObject> hud;
	private int id;
	private UiTextLabel labelName;
	private UiTextLabel labelLevel;
	private UiTextLabel labelScore;
	private UiTextLabel labelTimer;
	private UiButton btnHome;
	private UiText txtPause;
	private Timer timer;
	private Timer nextLevelTimer;
	private int level = 1;
	private int levelTime = 10;
	
	// input
	static int mouseX;
	static int mouseY;
	static boolean mouseClicked;
	static boolean keyUpDown;
	static boolean keyDownDown;
	static boolean keyLeftDown;
	static boolean keyRightDown;
	static boolean keyEnterPressed;
	static boolean keyEscPressed;
	static boolean keyPausePressed;

	public HUD(final int state) {
		this.id = state;
		hud = new ArrayList<>();
		labelName = new UiTextLabel("", 0, 0);
		hud.add(labelName);
		labelLevel = new UiTextLabel("", 0, 0);
		hud.add(labelLevel);
		labelScore = new UiTextLabel("", 0, 0);
		hud.add(labelScore);
		labelTimer = new UiTextLabel("", 0, 0);
		hud.add(labelTimer);
		btnHome = new UiButton("home", 0, 0);
		hud.add(btnHome);
		txtPause = new UiText("*** Pause ***", 0, 0);
		hud.add(txtPause);
		setPositions();
		timer = new Timer(0);
		nextLevelTimer = new Timer(1);
	}

	private void setPositions() {
		//calculate label width
		int w = Math.round((Game.width - btnHome.getWidth())/4.0f);
		int h;
		UiObject uio;
		//4 labels
		for (int i=0; i<4; i++) {
			uio = hud.get(i);
			h = uio.getHeight();
			uio.setImg(uio.getImg().getScaledCopy(w, h));
			uio.setPosition(i*w, Game.height-h);
		}
		//1 home button
		w = btnHome.getWidth();
		h = btnHome.getHeight();
		btnHome.setPosition(Game.width-w, Game.height-h);
		w = txtPause.getWidth();
		h += txtPause.getHeight();
		txtPause.setPosition((Game.width-w)/2, (Game.height-h)/2);
		txtPause.setVisible(false);
	}
	
	public int getLevel() {
		return level;
	}

	public void increaseLevel() {
		this.level++;
	}

	public void resetTimer() {
		timer.reset(levelTime);
	}

	public int getTime() {
		return timer.getSeconds();
	}
	
	public void setTime(int time) {
		levelTime = time;
	}

	public int getNextLevelTime() {
		return nextLevelTimer.getSeconds();
	}

	public void resetNextLevelTimer(int seconds) {
		nextLevelTimer.reset(seconds);;
	}

	public abstract void reset();
		
	private void handleInput(final GameContainer gc) {
		final Input input = gc.getInput();
		mouseClicked = input.isMousePressed(0);
		keyUpDown = input.isKeyDown(Input.KEY_UP);
		keyDownDown = input.isKeyDown(Input.KEY_DOWN);
		keyLeftDown = input.isKeyDown(Input.KEY_LEFT);
		keyRightDown = input.isKeyDown(Input.KEY_RIGHT);
		if (input.isKeyPressed(Input.KEY_ENTER) || input.isKeyPressed(Input.KEY_NUMPADENTER)) {
			keyEnterPressed = true;
		}
		keyEscPressed = input.isKeyPressed(Input.KEY_ESCAPE);
		keyPausePressed = input.isKeyPressed(Input.KEY_P);
		mouseX = input.getMouseX();
		mouseY = input.getMouseY();
	}

	@Override
	public void update(final GameContainer gc, final StateBasedGame sbg, final int dt) throws SlickException {
		handleInput(gc);
		if (btnHome.isMouseOver(mouseX, mouseY) && mouseClicked) {
			Game.gameState = Game.PAUSE;
			timer.pause();
			mouseClicked = false;
			sbg.enterState(Game.MENU_MAIN);
		}
		if (keyPausePressed) {
			if (Game.gameState == Game.PAUSE) {
				Game.gameState = Game.RESUME;
			} else {
				Game.gameState = Game.PAUSE;
				timer.pause();
			}
			keyPausePressed = false;
		}
		if (Game.gameState == Game.RESUME) {
			timer.start();
			nextLevelTimer.start();
			Game.gameState = Game.PLAY;
		}
		if (Game.gameState != Game.PAUSE) {
			if (Game.gameState == Game.NEW_GAME) {
				reset();
				Game.gameState = Game.PLAY;
				Game.gameIsRunning = true;
			}
			if (keyEscPressed) {
				Game.gameState = Game.PAUSE;
				timer.pause();
				keyEscPressed = false;
				sbg.enterState(Game.MENU_MAIN);
			}
			txtPause.setVisible(false);
		}else{
			txtPause.setVisible(true);
		}
		labelName.setText(Game.score.getName());
		labelLevel.setText("Level: "+level);
		labelScore.setText("Score: "+Game.score.getScore());
		nextLevelTimer.update();
		if (getNextLevelTime() == 0) timer.update();
		labelTimer.setText("Time: "+timer.getSeconds());
	}

	@Override
	public void render(final GameContainer gc, final StateBasedGame sbg, final Graphics g) throws SlickException {
		for (int i=0; i<hud.size(); i++) {
			hud.get(i).render(gc, g);
		}
	}

	@Override
	public int getID() {
		return id;
	}

}
