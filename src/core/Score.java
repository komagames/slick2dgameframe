package core;

import java.io.Serializable;

public class Score implements Serializable{
	private static final long serialVersionUID = 42L;
	private String name;
	private int score;
	
	public Score(){
		this.name = "unknown";
		this.score = 0;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void increaseScore(int amount) {
		this.score += amount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
