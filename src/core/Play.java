package core;

import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;
import org.newdawn.slick.util.Log;

public class Play extends HUD {
	// attributes for the Game
	private Color clBack = new Color(35,35,35);
	private int oldScore;
	private int maxLevel = 28;
	
	//Just for demonstration.
	private ArrayList<GameObject> level;
	private Player player;

	public Play(final int state) {
		super(state);
	}

	@Override
	public void init(final GameContainer gc, final StateBasedGame sbg) throws SlickException {
		player = new Player("player");
		buildLevel();
		Log.debug("Play.init()");
	}

	private void buildLevel() {
		level = new ArrayList<>();
		/* 10x6(x64px)=640x384
		 * x x x x x x x x x x
		 * x x x x x x x x x x
		 * x x x x x x x x x x
		 * x x x x x x x x x x
		 * x x x x x x x x x x
		 * x x x x x x x x x x
		 * 0 1 2 3 4 5 6 7 8 9
		 */
		int[] map = new int[60];
		Random rnd = new Random();
		GameObject go = null;
		int r = 0;
		boolean loop;
		int startX = (Game.width-640)/2;
		int startY = (Game.height-384)/2;
		for (int i=0; i<=getLevel()+2; i++) {
			level.add(new GameObject("target"));
			go = level.get(i);
			loop = true;
			while (loop) {
				r = rnd.nextInt(60);
				if (map[r] == 0) {
					map[r] = 1;
					loop = false;
				}
			}
			go.setPosition(r%10*64+startX, r/10*64+startY);
			go.getShape().setX(go.getX());
			go.getShape().setY(go.getY());
		}
		//last GameObject for player position (invisible) for reset
		go.setVisible(false);
		player.setPosition(go.getX(), go.getY());
		setTime(10+getLevel());
		resetNextLevelTimer(1);
	}

	public void reset() {
		player.reset();
		GameObject go = level.get(level.size()-1);
		player.setPosition(go.getX(), go.getY());
		Game.score.setScore(oldScore);
		resetTimer();
	}

	@Override
	public void update(final GameContainer gc, final StateBasedGame sbg, final int dt) throws SlickException {
		super.update(gc, sbg, dt);
		if (Game.gameState != Game.PAUSE) {
			// Update first! Then check intersection.
			if (getNextLevelTime() == 0) {
				player.update(gc, sbg, dt);
			}
			// Go backwards for correct detection!
			for (int i=level.size()-2; i>=0; i--) {
				if (player.intersects(level.get(i).getShape())) {
					if (i > 1) {
						Game.gameIsRunning = false;
						Game.gameState = Game.GAME_OVER;
						sbg.enterState(Game.GAME_OVER);
					}else if (i == 0){
						Game.score.increaseScore(1);
					}else{
						if (getLevel() < maxLevel) {
							oldScore = Game.score.getScore();
							increaseLevel();
							buildLevel();
							resetTimer();
						}else{
							Game.gameIsRunning = false;
							Game.highScore.addHighScore(Game.score);
							Game.gameState = Game.HIGHSCORE;
							sbg.enterState(Game.HIGHSCORE);
						}
					}
					break;
				}
			}
			if (getTime() == 0) {
				Game.gameIsRunning = false;
				if (Game.score.getScore() > 0) {
					Game.highScore.addHighScore(Game.score);
					Game.gameState = Game.HIGHSCORE;
					sbg.enterState(Game.HIGHSCORE);
				}else{
					Game.gameState = Game.GAME_OVER;
					sbg.enterState(Game.GAME_OVER);
				}
			}
		}
	}

	@Override
	public void render(final GameContainer gc, final StateBasedGame sbg, final Graphics g) throws SlickException {
		g.setBackground(clBack);
		g.setColor(Game.CL_TXT_HOVER);
		for (int i=0; i<level.size()-1; i++) {
			level.get(i).render(gc, sbg, g);
			//TODO: for testing - delete later
			g.setColor(Color.black);
			g.drawString("#"+i, level.get(i).getX(), level.get(i).getY());
		}
		player.render(gc, sbg, g);
		//render the HUD last
		super.render(gc, sbg, g);
	}

}
