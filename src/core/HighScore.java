package core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.Log;

import ui.UiTextLabel;
import ui.UiObject;
import ui.UiTextObject;

public class HighScore extends Menu {
	private ArrayList<Score> highScore;
	private static final int MAX_SIZE = 12;
	private int rank;
	private String fileName = "highscore.ser";

	public HighScore(final int state) {
		super(state);
		loadHighScore();
	}
	
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		//add labels for name and score
		for (int i=0; i<MAX_SIZE*2; i++) {
			menu.add(new UiTextLabel("", 0, 0));
		}
		setPositions();
		updateList();
		selection = 0;
		Log.debug("HighScore.init()");
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		super.render(gc, sbg, g);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int dt) throws SlickException {
		super.update(gc, sbg, dt);
		if (mouseClicked || keyEnterPressed) {
			switch (selection) {
			case 1:
				curPage--;
				if (curPage < 1) curPage = 1;
				break;
			case 2:
				curPage++;
				if (curPage > lastPage) curPage = lastPage;
				break;
			default: break;
			}
			keyEnterPressed = false;
		}
		if (keyEscPressed) {
			sbg.enterState(Game.MENU_MAIN);
			keyEscPressed = false;
		}
	}

	@Override
	public void setPositions() {
		//set positions and page
		int dist = 12;
		int h = 4*dist+logoHeight;
		int page = 1;
        int uioHeight = 0;
        UiObject uio1;
        UiObject uio2;
		int w = 0;
		if (page0Size < menu.size()) {
			w = menu.get(page0Size).getWidth();
		}
		int x1 = Game.width/2 - (int)(w*0.75);
		int x2 = x1 + w/2;
		for (int i=page0Size; i<menu.size(); i+=2) {
            uio1 = menu.get(i);
            uio2 = menu.get(i+1);
			uio1.setPosition(x2, h);
			uio2.setPosition(x1, h);
			uioHeight = uio1.getHeight();
			h += uioHeight;
			uio1.setPage(page);
			uio2.setPage(page);
			lastPage = page;
            //checking next page
            if (Game.height-h < uioHeight+2*dist) {
                page++;
                h = 4*dist+logoHeight;
            }
		}
	}

	public void addHighScore(Score score) {
		boolean newScore = false;
		rank = -1;
		for (int i=0; i<highScore.size(); i++){
			// Wenn neuer HighScore, dann an der Stelle einfügen
			if (score.getScore() > highScore.get(i).getScore()){
				highScore.add(i, score);
				rank = i;
				newScore = true;
				break;
			}
		}
		// Nur hinzufügen wenn kein neuer Score und noch Platz
		if (!newScore && highScore.size() <  MAX_SIZE){
			highScore.add(score);
			rank = highScore.size()-1;
		}
		// Letztes Element löschen wenn mehr als MAX_SIZE
		if (highScore.size() > MAX_SIZE){
			highScore.remove(highScore.size()-1);
		}
		updateList();
	}
	
	public void updateList() {
        UiTextObject labelName;
        UiTextObject labelScore;
        Score score;
        int l = page0Size;
        for (int s=0; s<highScore.size(); s++) {
            score = highScore.get(s);
            labelName = (UiTextObject)menu.get(l+1);
            labelScore = (UiTextObject)menu.get(l);
            labelName.setText(s+1+": "+score.getName());
            labelScore.setText(""+score.getScore());
            if (s == rank) {
                labelName.setClText(Game.CL_TXT_HOVER);
                labelScore.setClText(Game.CL_TXT_HOVER);
            }else {
                labelName.setClText(Game.CL_TXT_NORMAL);
                labelScore.setClText(Game.CL_TXT_NORMAL);
            }
            labelScore.setPosition(labelScore.getX(), labelScore.getY(), UiTextObject.H_RIGHT, UiTextObject.V_CENTER);
            l += 2;
        }
	}
	
	public void saveHighScore() {
		if (highScore.size() > 0) {
			FileOutputStream fos = null;
			ObjectOutputStream oos = null;
			try {
				File file = new File(fileName);
				if (!file.exists()) file.createNewFile();
				fos = new FileOutputStream(file);
				oos = new ObjectOutputStream(fos);
				oos.writeObject(highScore);
				oos.close();
				fos.close();
			}catch (final IOException ex) {
				Log.error("Error saving highscore!", ex);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void loadHighScore() {
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		highScore = null;
		try {
			File file = new File(fileName);
			if (file.exists()) {
				fis = new FileInputStream(file);
				ois = new ObjectInputStream(fis);
				Object obj = ois.readObject();
				ois.close();
				fis.close();
				if ( obj instanceof ArrayList<?>) {
					highScore = (ArrayList<Score>) obj;
				}else {
					highScore = null;
				}
			}
		}catch (final IOException | ClassNotFoundException ex) {
			Log.error("Error loading highscore!", ex);
		}finally {
			if (highScore == null) {
				highScore = new ArrayList<>();
			}
		}
	}

}
