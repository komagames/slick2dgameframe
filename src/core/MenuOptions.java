package core;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.Log;

import ui.UiTextButton;

public class MenuOptions extends Menu{

	public MenuOptions(final int state) {
		super(state);
	}

	@Override
	public void init(final GameContainer gc, final StateBasedGame sbg) throws SlickException {
		menu.add(new UiTextButton(Localisation.getString("MenuOptions.3"), 0, 0)); //Graphics
		menu.add(new UiTextButton(Localisation.getString("MenuOptions.4"), 0, 0)); //Sound
		menu.add(new UiTextButton(Localisation.getString("MenuOptions.5"), 0, 0)); //Controls
		menu.add(new UiTextButton(Localisation.getString("MenuOptions.6"), 0, 0)); //Language
		menu.add(new UiTextButton(Localisation.getString("MenuOptions.7"), 0, 0)); //Return
		setPositions();
		selection = 3; //Graphics
		Log.debug("MenuOptions.init()");
	}

	@Override
	public void render(final GameContainer gc, final StateBasedGame sbg, final Graphics g) throws SlickException {
		super.render(gc, sbg, g);
	}
	@Override
	public void update(final GameContainer gc, final StateBasedGame sbg, final int dt) throws SlickException {
		super.update(gc, sbg, dt);
		if (mouseClicked || keyEnterPressed) {
			switch (selection) {
			case 1:
				curPage--;
				if (curPage < 1) curPage = 1;
				break;
			case 2:
				curPage++;
				if (curPage > lastPage) curPage = lastPage;
				break;
			case 3: sbg.enterState(Game.MENU_GRAPHICS); break;
			case 4: break;
			case 5: break;
			case 6: sbg.enterState(Game.LANGUAGE); break;
			case 7: sbg.enterState(Game.MENU_MAIN); break;
			default: break;
			}
			mouseClicked = false;
			keyEnterPressed = false;
		}
		if (keyEscPressed) {
			sbg.enterState(Game.MENU_MAIN);
			keyEscPressed = false;
		}
	}
	
}
