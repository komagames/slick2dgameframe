package core;

import java.util.HashMap;

import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.util.Log;

public class Resources {
	private static HashMap<String, Image> images = new HashMap<>();
	private static HashMap<String, SpriteSheet> sprites = new HashMap<>();
	private static HashMap<String, Sound> sounds = new HashMap<>();
	private static HashMap<String, AngelCodeFont> fonts = new HashMap<>();
	
    static {
    	try {
    		images.put("logo", loadImage("resources/images/logo.png"));
    		images.put("label", loadImage("resources/images/label.png"));
    		images.put("target", loadImage("resources/images/target.png"));
    		sprites.put("home", loadSprite("resources/images/home.png", 48, 48));
			sprites.put("forward", loadSprite("resources/images/forward.png", 40, 48));
			sprites.put("backward", loadSprite("resources/images/backward.png", 40, 48));
			sprites.put("button", loadSprite("resources/images/button.png", 256, 48));
			sprites.put("textfield", loadSprite("resources/images/textfield.png", 256, 48));
			sprites.put("player", loadSprite("resources/images/player.png", 64, 64));
			fonts.put("bmf32", loadFont("resources/font/ARCena.fnt", "resources/font/ARCena.png"));
		} catch (SlickException e) {
			Log.error("Error loading resources!", e);
		}
    }

    private Resources() {
	}

	private static Image loadImage(String path) throws SlickException {
		//set FILTER_NEAREST for best scaling results
		return new Image(path, false, Image.FILTER_NEAREST);
	}

	private static SpriteSheet loadSprite(String path, int tw, int th) throws SlickException {
		return new SpriteSheet(loadImage(path), tw, th);
	}
	
	private static AngelCodeFont loadFont(String fntPath, String imgPath) throws SlickException {
		return new AngelCodeFont(fntPath, new Image(imgPath));
	}

	public static Image getImage(String key) {
		return images.get(key);
	}
	
	public static SpriteSheet getSprite(String key) {
		return sprites.get(key);
	}

	public static Image getSpriteImage(String key, int x, int y) {
		return sprites.get(key).getSubImage(x, y);
	}

	public static Sound getSound(String key) {
		return sounds.get(key);
	}
	
	public static AngelCodeFont getFont(String key) {
		return fonts.get(key);
	}
}
