package core;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.Log;

import ui.UiTextButton;
import ui.UiText;

public class GameOver extends Menu {

	public GameOver(final int state) {
		super(state);
	}

	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		menu.add(new UiText(Localisation.getString("GameOver.3"), 0, 0, Game.width/4, Game.height/4));
		menu.add(new UiTextButton(Localisation.getString("GameOver.4"), 0, 0)); //Reload
		menu.add(new UiTextButton(Localisation.getString("GameOver.5"), 0, 0)); //Main menu
		setPositions();
		selection = 4; //Reload
		Log.debug("GameOver.init()");
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		super.render(gc, sbg, g);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int dt) throws SlickException {
		super.update(gc, sbg, dt);
		if (mouseClicked || keyEnterPressed) {
			switch (selection) {
			case 4:
				Game.gameState = Game.NEW_GAME;
				sbg.enterState(Game.PLAY);
				break;
			case 5: 
				sbg.enterState(Game.MENU_MAIN); 
				break;
			default: break;
			}
			keyEnterPressed = false;
		}
	}

}
