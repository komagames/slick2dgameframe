package core;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.Log;

import ui.UiTextButton;

public class MenuLoad extends Menu {

	public MenuLoad(final int state) {
		super(state);
	}

	@Override
	public void init(final GameContainer container, final StateBasedGame game) throws SlickException {
		for (int i=1; i<=17; i++) {
			menu.add(new UiTextButton("Test"+i, 0, 0));
		}
		setPositions();
		selection = 1; //Test1
		Log.debug("MenuLoad.init()");
	}

	@Override
	public void render(final GameContainer gc, final StateBasedGame sbg, final Graphics g) throws SlickException {
		super.render(gc, sbg, g);
	}
	@Override
	public void update(final GameContainer gc, final StateBasedGame sbg, final int dt) throws SlickException {
		super.update(gc, sbg, dt);
		if (mouseClicked || keyEnterPressed) {
			switch (selection) {
			case 1:
				curPage--;
				if (curPage < 1) curPage = 1;
				break;
			case 2:
				curPage++;
				if (curPage > lastPage) curPage = lastPage;
				break;
			case 3: break;
			case 4: break;
			case 5: break;
			case 6: break;
			case 7: break;
			default: break;
			}
			mouseClicked = false;
			keyEnterPressed = false;
		}
		if (keyEscPressed) {
			sbg.enterState(Game.MENU_MAIN);
			keyEscPressed = false;
		}
	}
}
