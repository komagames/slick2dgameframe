package core;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;
import org.newdawn.slick.util.Log;

public class Game extends StateBasedGame {
	// Application constants
	// Programm-Konstanten
	public static final String TITLE = "Slick2DGame";
	public static final String VERSION = "1.0";
	public static final Color CL_TXT_NORMAL = Color.white;
	public static final Color CL_TXT_HOVER = new Color(255,128,0);
	public static final Color CL_TXT_CHANGED = Color.yellow;
	public static final int FPS = 60;

	// Game state identifiers
	// Spiel-Status-Bezeichner
	public static final int MENU_MAIN = 0;
	public static final int MENU_OPTIONS = 1;
	public static final int LANGUAGE = 2;
	public static final int NEW_GAME = 4;
	public static final int PLAY = 5;
	public static final int PAUSE = 6;
	public static final int RESUME = 7;
	public static final int HIGHSCORE = 8;
	public static final int GAME_OVER = 9;
	public static final int MENU_SAVE = 11;
	public static final int MENU_LOAD = 12;
	public static final int MENU_GRAPHICS = 13;
	
	// Application Properties
	// Programm-Eigenschaften
	public static AppGameContainer app;
	public static int width = 800;
	public static int height = 600;
	public static Font BMF32;
	public static boolean gameIsRunning;
	public static int gameState;
	public static HighScore highScore;
	public static Score score;
	public static boolean showFPS = true;
	public static boolean vSync = true;

	// Constructor: sets the title
	// Konstruktor: setzt den Titel
	public Game(String title) {
		super(title);
	}

	// Initialize your game states (calls init method of each gamestate
	// and set's the state ID)
	// Initialisiere jeden Spiel-Status (ruft die init-Methode für jeden
	// Spiel-Status auf und setzt die Status-ID)
	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		// sets the Framerate of your Game to a fix value
		// setzt die Framerate Deines Spiels auf einen festen Wert
		gc.setTargetFrameRate(FPS);
		// also render in background
		// auch im Hintergrund rendern
		gc.setAlwaysRender(true);
		// true = shows FPS / false = shows not FPS
		// ture = zeigt die FPS / false = zeigt nicht die FPS
		gc.setShowFPS(showFPS);
        // sets Vsync to true
        // setzt Vsync auf true
		gc.setVSync(vSync);
		//set font first
		initFont();
		// The first state added will be the one that is loaded first, 
		// when the application is launched.
		// Der erste hinzugefügte Status ist der, der als Erstes geladen
		// wird, wenn das Programm startet.
		this.addState(new MenuMain(MENU_MAIN));
		this.addState(new MenuOptions(MENU_OPTIONS));
		this.addState(new Language(LANGUAGE));
		this.addState(new MenuNewGame(NEW_GAME));
		this.addState(new Play(PLAY));
		highScore = new HighScore(HIGHSCORE);
		this.addState(highScore);
		this.addState(new GameOver(GAME_OVER));
		this.addState(new MenuLoad(MENU_LOAD));
		this.addState(new MenuGraphics(MENU_GRAPHICS));
		// Call enterState() to load a specific state.
		// Rufe enterState() auf, um einen bestimmten Status zu laden.
		this.enterState(MENU_MAIN);
		gameState = NEW_GAME;
		Log.debug("Game.initStatesList()");
	}

	// The main method to launch the application.
	// Die Main-Methode zum Starten des Programms.
	public static void main(String[] args) {
		try {
			// Create an AppGameContainer an call the constructor
			// of your main class -> Game
			// Erstelle einen AppGameContainer und rufe den Konstruktor
			// Deiner Hauptklasse auf -> Game
			app = new AppGameContainer(new Game(TITLE+" - "+VERSION));
			// sets the resolution of your game
			// setzt die Auflösung Deines Spiels
			app.setDisplayMode(width, height, false);
            // starts the game
            // Startet das Spiel
			Log.debug("start game");
			app.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean closeRequested() {
		highScore.saveHighScore();
		return true;
	}
	
	public static void closeGame(GameContainer gc) {
		highScore.saveHighScore();
		gc.exit();
	}

	public void initFont() throws SlickException{
		BMF32 = Resources.getFont("bmf32");
	}
}
