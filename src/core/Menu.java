package core;

import java.util.ArrayList;
import java.util.Locale;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import ui.UiButton;
import ui.UiLogo;
import ui.UiObject;
import ui.UiTextObject;

public abstract class Menu extends BasicGameState {
	ArrayList<UiObject> menu;
	private int id;
	private Color clBack = new Color(35,35,35);
	private Color clText = Game.CL_TXT_NORMAL;
	private Locale locale;
	private static Locale newLocale;
	int page0Size;
	int page0Selection;
	int selection;
	int curPage = 1;
	int lastPage;
	int firstButton;
	int lastButton;

	// input
	static int mouseX;
	static int mouseY;
	static boolean mouseClicked;
	static boolean mouseDown;
	static boolean mouseOver;
	static boolean keyUpPressed;
	static boolean keyDownPressed;
	static boolean keyLeftPressed;
	static boolean keyRightPressed;
	static boolean keyEnterPressed;
	static boolean keyEnterDown;
	static boolean keyEscPressed;

	// game logo height
	static int logoHeight;

    public Menu(final int state) {
		this.id = state;
		menu = new ArrayList<>();
		UiLogo logo = new UiLogo(0,0);
		logoHeight = logo.getImg().getHeight();
		logo.setPosition((Game.width-logo.getWidth())/2, 24);
		menu.add(logo);
		UiButton btn = new UiButton("backward",0,0);
		btn.setPosition(0, (Game.height-btn.getWidth())/2);
		btn.setVisible(false);
		menu.add(btn);
		btn = new UiButton("forward",0,0);
		btn.setPosition(Game.width-btn.getWidth(), (Game.height-btn.getWidth())/2);
		btn.setVisible(false);
		menu.add(btn);
		page0Size = menu.size();
		locale = Localisation.getLocale();
		if (getLocale().getLanguage() == "") locale = Locale.getDefault();
		newLocale = locale;
	}

	private void handleInput(final GameContainer gc) {
		final Input input = gc.getInput();
		mouseClicked = input.isMousePressed(0);
		mouseDown = input.isMouseButtonDown(0);
		keyUpPressed = input.isKeyPressed(Input.KEY_UP);
		keyDownPressed = input.isKeyPressed(Input.KEY_DOWN);
		keyLeftPressed = input.isKeyPressed(Input.KEY_LEFT);
		keyRightPressed = input.isKeyPressed(Input.KEY_RIGHT);
		if (input.isKeyPressed(Input.KEY_ENTER) || input.isKeyPressed(Input.KEY_NUMPADENTER)) {
			keyEnterPressed = true;
		}else keyEnterPressed = false;
		if (input.isKeyDown(Input.KEY_ENTER) || input.isKeyDown(Input.KEY_NUMPADENTER)) {
			keyEnterDown = true;
		}else keyEnterDown = false;
		keyEscPressed = input.isKeyPressed(Input.KEY_ESCAPE);
		mouseX = input.getMouseX();
		mouseY = input.getMouseY();
	}

	@Override
	public void update(final GameContainer gc, final StateBasedGame sbg, final int dt) throws SlickException {
		handleInput(gc);
		mouseOver = false;
		int menuSize = menu.size();
		//checking for selection on page 0
		if (keyLeftPressed) {
			curPage--;
			if (curPage < 1) curPage = 1;
		}
		if (keyRightPressed) {
			curPage++;
			if (curPage > lastPage) curPage = lastPage;
		}
		for (int i = 0; i<page0Size; i++) {
			if (menu.get(i).isMouseOver(mouseX, mouseY)) {
				selection = i;
				mouseOver = true;
			}
		}
		//set visibility for page scroll buttons
		if (curPage == 1) menu.get(1).setVisible(false);
		else menu.get(1).setVisible(true);
		if (curPage == lastPage) menu.get(2).setVisible(false);
		else menu.get(2).setVisible(true);
		//set firstButton and lastButton based on the page
		int oldPage = 0;
		UiObject uio;
		for (int i=0; i<menuSize; i++) {
			uio = menu.get(i);
			int page = uio.getPage();
			if (page == curPage && uio.isSelectable() && uio.isVisible()) {
				if (oldPage < page) {
					firstButton = i;
					oldPage = page;
				}
				lastButton = i;
			}else if (page > curPage) {
				break;
			}
		}
		//set selection
		if (keyDownPressed) {
			while (selection < lastButton) {
				selection++;
				if (menu.get(selection).isSelectable()) break;
			}
			keyDownPressed = false;
		}else if (keyUpPressed) {
			while (selection > firstButton) {
				selection--;
				if (menu.get(selection).isSelectable()) break;
			}
			keyUpPressed = false;
		}else {
			for (int i = firstButton; i <= lastButton; i++) {
				if (menu.get(i).isMouseOver(mouseX, mouseY)) {
					selection = i;
					mouseOver = true;
				}
			}
			if (!mouseOver) {
				mouseClicked = false;
			}
		}
		//set focus and active
		menu.get(selection).setFocus(true);
		if ((mouseOver && mouseDown) || keyEnterPressed){
			menu.get(selection).setActivated(true);
		}
		//update buttons
		for (int i = 0; i<page0Size; i++) {
			menu.get(i).update();
		}
		for (int i = firstButton; i <= lastButton; i++){
			menu.get(i).update();
		}
		//change language
		if (!locale.getLanguage().equals(newLocale.getLanguage())) {
			locale = newLocale;
			for (int i=0; i<menuSize; i++) {
				if (menu.get(i) instanceof UiTextObject) {
					String text = Localisation.getString(getClass().getSimpleName()+"."+i);
					if (text != null) ((UiTextObject)menu.get(i)).setText(text);
				}
			}
		}
	}

	@Override
	public void render(final GameContainer gc, final StateBasedGame sbg, final Graphics g) throws SlickException {
		g.setBackground(clBack);
		g.setColor(clText);
		UiObject uio;
		int page;
		for (int i = 0; i < menu.size(); i++) {
			uio = menu.get(i);
			page = uio.getPage();
			if ((page == 0 || page == curPage)) {
				uio.render(gc, g);
			}
		}
	}

	@Override
	public int getID() {
		return id;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		newLocale = locale;
		Localisation.setLocale(locale);
	}

	public void setPositions() {
		int dist = 12;
		//2*dist above and 2*dist under the logo
		int h = 4*dist+logoHeight;
		int page = 1;
		int uioHeight = 0;
		UiObject uio;
		for (int i = page0Size; i < menu.size(); i++){
			uio = menu.get(i);
			uio.setPage(page);
			if (uio.getX() == 0 && uio.getY() == 0) {
				uio.setPosition((Game.width-uio.getWidth())/2, h);
				uioHeight = uio.getHeight();
				h += uioHeight+dist;
				lastPage = page;
				//checking next page
				if (Game.height-h < uioHeight+2*dist) {
					page++;
					h = 4*dist+logoHeight;
				}
			}
		}
	}

}
