package core;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.state.StateBasedGame;

public class Player extends GameObject{
	private Image playerFront;
	private Image playerRight;
	private Image playerBack;
	private Image playerLeft;
	private float speed;

	public Player() {
		this(null, 0, 0);
	}
	
	public Player(String key) {
		this(key, 0, 0);
	}
	
	public Player(String key, int x, int y) {
		super(key, x, y);
		playerFront = getImg(0, 0);
		playerRight = getImg(1, 0);
		playerBack = getImg(2, 0);
		playerLeft = getImg(3, 0);
		setShape(new Circle(getX(), getY(), getWidth()/2));
		reset();
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		super.render(gc, sbg, g);
	}
	
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int dt) throws SlickException {
		if (Play.keyDownDown) {
			setImg(playerFront);
			setY(getY()+(int)(speed*dt));
		}
		if (Play.keyRightDown) {
			setImg(playerRight);
			setX(getX()+(int)(speed*dt));
		}
		if (Play.keyUpDown) {
			setImg(playerBack);
			setY(getY()-(int)(speed*dt));
		}
		if (Play.keyLeftDown) {
			setImg(playerLeft);
			setX(getX()-(int)(speed*dt));
		}
		getShape().setX(getX());
		getShape().setY(getY());
	}
	
	public boolean intersects(Shape shape) {
		return getShape().intersects(shape);
	}
	
	public void reset() {
		setImg(playerFront);
		setX(Game.width/2);
		setY(Game.height/2);
		speed = 0.18f;
	}
}
