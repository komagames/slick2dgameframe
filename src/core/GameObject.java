package core;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.state.StateBasedGame;

public class GameObject {
	private SpriteSheet sprite;
	private Image img;
	private Shape shape;
	private int x;
	private int y;
	private int width;
	private int height;
	private boolean visible;

	public GameObject() {
		this(null, 0, 0);
	}
	
	public GameObject(String key) {
		this(key, 0, 0);
	}
	
	public GameObject(String key, int x, int y) {
		img = Resources.getImage(key);
		if (img == null) {
			sprite = Resources.getSprite(key);
			img = getImg(0, 0);
		}
		if (img != null) {
			width = img.getWidth();
			height = img.getHeight();
		}
		this.x = x;
		this.y = y;
		visible = true;
		shape = new Rectangle(x, y, width, height);
	}

	public Image getImg() {
		return img;
	}

	public Image getImg(int x, int y) {
		return sprite.getSprite(x, y);
	}
	
	public void setImg(Image img) {
		this.img = img;
	}

	public Shape getShape() {
		return shape;
	}

	public void setShape(Shape shape) {
		this.shape = shape;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		g.drawImage(img, x, y);
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int dt) throws SlickException {
		
	}
}
