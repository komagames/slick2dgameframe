package ui;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public abstract class UiObject {
	private Image img;
	private String hint;
	private int x;
	private int y;
	private int width;
	private int height;
	private int page;
	private boolean visible;
	private boolean selectable;
	private boolean activated;

	private UiObject(Image img) {
		this.img = img;
		this.hint = "";
		if (img != null) {
			this.width = img.getWidth();
			this.height = img.getHeight();
		}
		this.visible = true;
	}

	public UiObject(Image img, int x, int y) {
		this(img);
		this.setPosition(x, y);
	}

	public Image getImg() {
		return img;
	}

	public void setImg(Image img) {
		this.img = img;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isSelectable() {
		return selectable;
	}

	public void setSelectable(boolean selectable) {
		this.selectable = selectable;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public void update() {
	}
	
	public void render(GameContainer gc, Graphics g) {
		if (img != null && isVisible()) g.drawImage(img, x, y);
	}

	public boolean isMouseOver(int mx, int my) {
		boolean result = false;
		if (selectable) {
			if (mx >= getX() && mx <= getX()+getWidth() && 
					my >= getY() && my <= getY()+getHeight()) {
				setFocus(true);
				result = true;
			}else{
				setFocus(false);
			}
		}
		return result;
	}

	public void setFocus(boolean focus) {
	}
}
