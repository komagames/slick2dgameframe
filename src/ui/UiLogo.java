package ui;

import core.Resources;

public class UiLogo extends UiObject{

	public UiLogo(int x, int y) {
		super(Resources.getImage("logo"), x, y);
	}

}
