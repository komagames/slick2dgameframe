package ui;

import core.Resources;

public class UiTextLabel extends UiTextObject{

	public UiTextLabel(String text, int x, int y) {
		super(Resources.getImage("label"), text, x, y, UiTextObject.H_LEFT, UiTextObject.V_CENTER);
	}

	public UiTextLabel(String text, int x, int y, int alignH, int alignV) {
		super(Resources.getImage("label"), text, x, y, alignH, alignV);
	}

}
