package ui;

import org.newdawn.slick.Image;

import core.Resources;

public class UiButton extends UiObject {
	private Image imgNormal;
	private Image imgHover;
	private Image imgPressed;


    public UiButton(String key, int x, int y) {
		super(Resources.getSpriteImage(key, 0, 0), x, y);
		imgNormal = Resources.getSpriteImage(key, 0, 0);
		imgHover = Resources.getSpriteImage(key, 1, 0);
		imgPressed = Resources.getSpriteImage(key, 2, 0);
		setSelectable(true);
    }

	@Override
	public void setActivated(boolean activated) {
		if (activated) setImg(imgPressed); 
	}

	@Override
	public void setFocus(boolean focus) {
		if (focus) {
			setImg(imgHover);
		}else{
			setImg(imgNormal);
		}
	}
}
