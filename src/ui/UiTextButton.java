package ui;

import org.newdawn.slick.Image;

import core.Game;
import core.Resources;

public class UiTextButton extends UiTextObject {
	private static Image imgNormal;
	private static Image imgHover;
	private static Image imgPressed;

    public UiTextButton(String text, int x, int y) {
    	super(Resources.getSpriteImage("button", 0, 0), text, x, y);
    	setImages();
		setSelectable(true);
	}

	public UiTextButton(String text, int x, int y, int alignH, int alignV) {
		super(Resources.getSpriteImage("button", 0, 0), text, x, y, alignH, alignV);
		setImages();
		setSelectable(true);
	}

	private void setImages() {
		imgNormal = Resources.getSpriteImage("button", 0, 0);
		imgHover = Resources.getSpriteImage("button", 0, 1);
		imgPressed = Resources.getSpriteImage("button", 0, 2);
	}

	@Override
	public void setActivated(boolean activated) {
		if (activated) setImg(imgPressed); 
	}

	@Override
	public void setFocus(boolean focus) {
		if (focus) {
			setImg(imgHover);
			setClText(Game.CL_TXT_HOVER);
		}else{
			setImg(imgNormal);
			setClText(Game.CL_TXT_NORMAL);
		}
	}
}
