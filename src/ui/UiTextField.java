package ui;


import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.gui.TextField;

import core.Game;
import core.Resources;

public class UiTextField extends UiTextObject {
	private TextField textField;
	private static Image imgNormal;
	private static Image imgFocus;
	// set color to invisible to draw the image
	private Color clNone = new Color(0,0,0,0);

	public UiTextField(String text, GameContainer gc, int x, int y) {
		super(Resources.getSpriteImage("textfield", 0, 0), text, x, y);
		imgNormal = Resources.getSpriteImage("textfield", 0, 0);
		imgFocus = Resources.getSpriteImage("textfield", 0, 1);
		this.createTextField(gc);
		setSelectable(true);
	}

	private void createTextField(GameContainer gc) {
		textField = new TextField(gc, getFont(), getX(), getY(), getWidth(), getHeight());
		textField.setBackgroundColor(clNone);
		textField.setBorderColor(clNone);
		textField.setTextColor(Game.CL_TXT_NORMAL);
		textField.setMaxLength(12);
		textField.setText(getText());
		textField.setCursorPos(getText().length());
	}
	
	@Override
	public void setPosition(int x, int y) {
		super.setPosition(x, y);
		if (textField != null) textField.setLocation(x, y);
	}

	@Override
	public void setActivated(boolean activated) {
		if (isActivated()) {
			super.setActivated(false);
			textField.setFocus(false);
		}else {
			super.setActivated(true);
			textField.setFocus(true);
		}
		setFocus(true);
	}

	@Override
	public void update() {
		setText(textField.getText());
	}
	
	@Override
	public void render(GameContainer gc, Graphics g) {
		if (getImg() != null) g.drawImage(getImg(), getX(), getY());
		textField.render(gc, g);
	}

	@Override
	public void setFocus(boolean focus) {
		if (textField.hasFocus() || focus) {
			textField.setTextColor(Game.CL_TXT_HOVER);
			setImg(imgFocus);
		}else{
			textField.setTextColor(Game.CL_TXT_NORMAL);
			setImg(imgNormal);
		}
	}
}
