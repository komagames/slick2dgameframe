package ui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import core.Game;

public abstract class UiTextObject extends UiObject{
	public static final int H_LEFT = 0;
	public static final int H_RIGHT = 1;
	public static final int H_CENTER = 2;
	public static final int V_TOP = 3;
	public static final int V_BOTTOM = 4;
	public static final int V_CENTER = 5;
	
	private Font font;
	private Color clText;
	private String text;
	private int textX;
	private int textY;
	private int alignH;
	private int alignV;

	public UiTextObject(Image img, String text, int x, int y) {
		this(img, text, x, y, H_CENTER, V_CENTER);
	}
	public UiTextObject(Image img, String text, int x, int y, int textAlignH, int textAlignV) {
		super(img, x, y);
		this.text = text;
		this.clText = Game.CL_TXT_NORMAL;
		this.font = Game.BMF32;
		this.alignH = textAlignH;
		this.alignV = textAlignV;
		this.setPosition(x, y, textAlignH, textAlignV);
	}

	public Font getFont() {
		return font;
	}

	public void setFont(Font font) {
		this.font = font;
	}

	public Color getClText() {
		return clText;
	}

	public void setClText(Color clText) {
		this.clText = clText;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
		this.setPosition();
	}

	public void setPosition() {
		this.setPosition(getX(), getY(), alignH, alignV);
	}

	public void setPosition(int x, int y) {
		this.setPosition(x, y, alignH, alignV);
	}

	public void setPosition(int x, int y, int alignH, int alignV) {
		super.setPosition(x, y);
		this.alignH = alignH;
		this.alignV = alignV;
		if (font != null) {
			int w = font.getWidth(text);
			int h = font.getLineHeight();
			int newX = x;
			int newY = y;
			switch (alignH) {
			case H_LEFT: newX = x + 5; break;
			case H_RIGHT: newX = x + getWidth() - w - 5; break;
			case H_CENTER: newX = x + getWidth()/2 - w/2; break;
			default: break;
			}
			switch (alignV) {
			case V_TOP: break;
			case V_BOTTOM: newY = y + getHeight() - h; break;
			case V_CENTER: newY = y + getHeight()/2 - h/2;
			default: break;
			}
			this.textX = newX;
			this.textY = newY;
		}
	}

	@Override
	public void render(GameContainer gc, Graphics g) {
		super.render(gc, g);
		if (font != null && isVisible()) font.drawString(textX, textY, text, clText);
	}
	
}
