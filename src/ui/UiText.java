package ui;

public class UiText extends UiTextObject {
/**
 * This is an UiTextObject with an invisible label. Set the width and height
 * for the invisible label and position the text within the invisible label
 * using the align attributes. The invisible label can be much bigger than
 * the text to arrange other ui objects around this.
 * 
 * @author Jeanne d'Art
 */

	/**
	 * Constructs an {@link UiTextObject} with an invisible label.
	 * Width an height will be calculated from the dimensions of
	 * the text using the own font.
	 * 
	 * @param text - The text to be displayed.
	 * @param x - The x position of the invisible label.
	 * @param y - The y position of the invisible label.
	 * 
	 * @since 08-20-2017
	 */
	public UiText(String text, int x, int y) {
		super(null, text, x, y);
		setWidth(getFont().getWidth(text));
		setHeight(getFont().getHeight(text));
	}

	/**
	 * Constructs an {@link UiTextObject} with an invisible label.
	 * 
	 * @param text - The text to be displayed.
	 * @param x - The x position of the invisible label.
	 * @param y - The y position of the invisible label.
	 * @param w - The width of the invisible label.
	 * @param h - The height of the invisible label.
	 * 
	 * @since 08-06-2017
	 */
	public UiText(String text, int x, int y, int w, int h) {
		super(null, text, x, y);
		setWidth(w);
		setHeight(h);
	}

}
